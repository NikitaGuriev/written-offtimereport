<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/core/css/core.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/intranet/intranet-common.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/fonts/opensans/ui.font.opensans.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/sidepanel/css/sidepanel.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/intranet/sidepanel/bindings/mask.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/intranet/sidepanel/bitrix24/slider.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/popup/dist/main.popup.bundle.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/socialnetwork/common/socialnetwork.common.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/colorpicker/css/colorpicker.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/intranet/theme_picker/theme_picker.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/tasks/css/media.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/loader/dist/loader.bundle.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/core/css/core_viewer.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/tasks/css/tasks.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/core/css/core_date.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/intranet/core_planner.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/calendar/core_planner_handler.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/tooltip/tooltip.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/core/css/core_tooltip.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/timeman/timemanager/css/core_timeman.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/core/css/core_finder.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/helper/css/helper.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/main/phonenumber/css/phonenumber.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/actionpanel/css/style.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/buttons/src/css/ui.buttons.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/buttons/src/css/ui.buttons.ie.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/buttons/icons/ui.buttons.icons.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/buttons/icons/ui.buttons.icons.ie.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/viewer/css/style.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/im/css/common.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/im/css/dark_im.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/rest/css/applayout.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/im/css/phone_call_view.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/notification/ui.notification.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/alerts/ui.alerts.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/disk/css/disk.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/disk/css/file_dialog.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/im/css/window.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/im/css/im.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/im/css/call/view.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/tour/ui.tour.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/tutor/ui.tutor.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/js/ui/info-helper/css/info-helper.min.css' ?>" type="text/css" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/components/bitrix/tasks.iframe.popup/templates/.default/style.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/themes/.default/clock.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/components/bitrix/search.title/.default/style.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/groups.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/map.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/style.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/components/bitrix/crm.card.show/templates/.default/style.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/components/bitrix/im.messenger/.default/style.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/components/bitrix/bitrix24.notify.panel/templates/.default/style.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/template_styles.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/interface.min.css' ?>" type="text/css" data-template-style="true" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/themes/light/main.css' ?>" type="text/css" media="screen" data-template-style="true" data-theme-id="light:atmosphere" rel="stylesheet">
<link href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/themes/light/menu.css' ?>" type="text/css" media="screen" data-template-style="true" data-theme-id="light:atmosphere" rel="stylesheet">
<!--<link href="<?/*= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/themes/light/atmosphere/style.css' */?>" type="text/css" media="screen" data-template-style="true" data-theme-id="light:atmosphere" rel="stylesheet">-->
<link rel="stylesheet" type="text/css" media="print" href="<?= 'https://' . DOMAIN . '/bitrix/templates/bitrix24/print.css' ?>">