<?php
require(__DIR__ . '/settings.php');
function array2csv($array)
{
    if (count($array) == 0) return null;
    ob_start();
    $df = fopen("php://output", 'w');
    fputs($df, chr(0xEF) . chr(0xBB) . chr(0xBF));
    foreach ($array as $row) {
        fputcsv($df, $row, ';', '"');
    }
    fclose($df);
    return ob_get_clean();
}

function downloadSendHeaders($filename)
{
    // Отключить кэширование
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");
    // Принудительная загрузка
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    // Disposition/Encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

function formatArray($arr)
{
    $data = array(
        array(
            'Направление сделки',
            'Id сделки',
            'Сделка',
            'Ссылка на сделку',
            'Дата начала сделки',
            'Дата закрытия сделки',
            'Стадия сделки',
            'Id задачи',
            'Задача',
            'Стадия задачи (закрыта/не закрыта)',
            'Норма времени по задаче',
            'Списано времени по задаче',
            'Дата (время)',
            'Автор',
            'Время (списано)',
            'Комментарий (расшифровка что сделано)'
        )
    );
    foreach ($arr as $k => $value) {
        $tmp[] = array(
            $value['dealDirection'],
            $value['dealID'],
            $value['dealName'],
            $value['dealLink'],
            $value['dealStartDate'],
            $value['dealCloseDate'],
            $value['dealStage'],
            $value['taskID'],
            $value['taskLink'],
            $value['taskStage'],
            $value['taskTimeEstimate'],
            $value['taskWrittenTime'],
            $value['commentDate'],
            $value['commentAuthor'],
            $value['commentWrittenTime'],
            $value['commentText']
        );
    }
    $data = array_merge($data, $tmp);
    return $data;
}

downloadSendHeaders('wotrExport_' . date('Y-m-d-h-i') . '.csv');
echo array2csv(formatArray($appsConfig));
die();