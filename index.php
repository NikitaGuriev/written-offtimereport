<?php
### Настройки ###
require_once __DIR__ . '/libs/crest/CRestPlus.php';
require_once __DIR__ . '/libs/debugger/Debugger.php';

### Определение констант ###
define('DOMAIN', $_REQUEST['DOMAIN']);
define('CLIENT', __DIR__ . '/libs/crest/settings.json');
define('CURRENT_DATE', date('Y-m-d'));
define('LAST_MONTH', date('Y-m-d', strtotime(CURRENT_DATE . " - 1 month")));
define('INFO_BLOCK', '3'); // Статус инфомрационного блока (1 - в стадии разработки (develop), 2 - в стадии дебаггинга (debug), 3 - в стадии бета-тестирования (beta), 0 - убрать инфомрационный блок)

### Установка приложения ###
if (isset($_REQUEST['PLACEMENT']) && !file_exists(CLIENT)) require_once __DIR__ . '/libs/crest/install.php';

### Алгоритм работы ###
$usersCall = CRestPlus::callBatchList('user.get', array(
    'sort' => 'LAST_NAME',
    'order' => 'asc',
    'filter' => array(
        'USER_TYPE' => 'employee',
        'ACTIVE' => 'Y'
    )
));
foreach ($usersCall['result']['result'] as $ucPackage) {
    foreach ($ucPackage as $ucItem) {
        $users[$ucItem['ID']] = $ucItem['LAST_NAME'] . ' ' . $ucItem['NAME'];
    }
}
$statusesCall = CRestPlus::callBatchList('crm.status.list', array());
$dealsCats['0'] = 'Общее';
$dclCBL = CRestPlus::callBatchList('crm.dealcategory.list', array());
foreach ($dclCBL['result']['result'] as $dcblPackages) {
    foreach ($dcblPackages as $dcblItem) {
        $dealsCats[$dcblItem['ID']] = $dcblItem['NAME'];
    }
}
foreach ($statusesCall['result']['result'] as $scPackages) {
    foreach ($scPackages as $scItems) {
        if (preg_match('~DEAL_STAGE~', $scItems['ENTITY_ID'])) {
            $statIDs[$scItems['STATUS_ID']] = '[' . $dealsCats[0] . '] ' . $scItems['NAME'];
            if (preg_match('~DEAL_STAGE_~', $scItems['ENTITY_ID'])) {
                $explSI = explode(':', $scItems['STATUS_ID']);
                foreach ($dealsCats as $dCatsKey => $dCatsValues) {
                    if (($dCatsKey == substr($explSI[0], 1))) {
                        $statIDs[$scItems['STATUS_ID']] = '[' . $dCatsValues . '] ' . $scItems['NAME'];
                    }
                }
            }
        }
    }
}
if (!is_null($_POST['author'])) {
    if ((!empty($_POST['dateStart']) and (!empty($_POST['dateEnd'])))) {
        $dateStart = $_POST['dateStart'] . 'T00:00:00+03:00';
        $dateEnd = $_POST['dateEnd'] . 'T23:59:59+03:00';
    } else {
        $dateStart = LAST_MONTH . 'T00:00:00+03:00';
        $dateEnd = CURRENT_DATE . 'T23:59:59+03:00';
    }
    $curUsr = CRestPlus::call('user.current');
    ### Расчёт количества страниц для метода task.elapseditem.getlist ###
    if ($_POST['author'] === 'empty') {
        $totalTEG = CRestPlus::call('task.elapseditem.getlist', array(
                'ORDER' => array(
                    'USER_ID' => 'asc',
                    'TASK_ID' => 'asc',
                    'CREATED_DATE' => 'asc'
                ),
                'FILTER' => array(
                    '>CREATED_DATE' => $dateStart,
                    '<CREATED_DATE' => $dateEnd
                ))
        );
    } else {
        $totalTEG = CRestPlus::call('task.elapseditem.getlist', array(
                'ORDER' => array(
                    'USER_ID' => 'asc',
                    'TASK_ID' => 'asc',
                    'CREATED_DATE' => 'asc'
                ),
                'FILTER' => array(
                    'USER_ID' => $_POST['author'],
                    '>CREATED_DATE' => $dateStart,
                    '<CREATED_DATE' => $dateEnd
                ))
        );
    }
    if ($totalTEG !== 'error') {
        $totalWOT = $totalTEG['total'];
        $pages = 0;
        while ($totalWOT > 0) {
            if ((($totalWOT % 50) !== 0) and ($totalWOT !== 0)) {
                $totalWOT = $totalWOT - ($totalWOT % 50);
                $pages++;
            } elseif ((($totalWOT % 50) === 0) and ($totalWOT !== 0)) {
                $totalWOT = $totalWOT - 50;
                $pages++;
            }
        }
        if ($_POST['author'] === 'empty') {
            ### Выборка данных для таблицы ###
            for ($eI = 1; $eI <= $pages; $eI++) {
                $tasksTime[] = CRestPlus::call('task.elapseditem.getlist', array(
                    'ORDER' => array(
                        'USER_ID' => 'asc',
                        'TASK_ID' => 'asc',
                        'CREATED_DATE' => 'asc'
                    ),
                    'FILTER' => array(
                        '>CREATED_DATE' => $dateStart,
                        '<CREATED_DATE' => $dateEnd
                    ),
                    'SELECT' => array('TASK_ID', 'CREATED_DATE', 'USER_ID', 'MINUTES', 'COMMENT_TEXT'),
                    'PARAMS' => array(
                        'NAV_PARAMS' => array(
                            'nPageSize' => '50',
                            'iNumPage' => $eI
                        )
                    )
                ));
            }
        } else {
            for ($i = 1; $i <= $pages; $i++) {
                $tasksTime[] = CRestPlus::call('task.elapseditem.getlist', array(
                    'ORDER' => array(
                        'USER_ID' => 'asc',
                        'TASK_ID' => 'asc',
                        'CREATED_DATE' => 'asc'
                    ),
                    'FILTER' => array(
                        'USER_ID' => $_POST['author'],
                        '>=CREATED_DATE' => $dateStart,
                        '<=CREATED_DATE' => $dateEnd
                    ),
                    'SELECT' => array('TASK_ID', 'CREATED_DATE', 'USER_ID', 'MINUTES', 'COMMENT_TEXT'),
                    'PARAMS' => array(
                        'NAV_PARAMS' => array(
                            'nPageSize' => '50',
                            'iNumPage' => $i
                        )
                    )
                ));
            }
        }
        if ($tasksTime[0]['total'] === '0') {
            $stageNF = true;
        } else {
            if ($tasksTime !== 'error') {
                foreach ($tasksTime as $ttFLPackage) {
                    foreach ($ttFLPackage['result'] as $ttFLValues) {
                        $tasksIDs[] = $ttFLValues['TASK_ID'];
                    }
                }
                $tuIDs = array_unique($tasksIDs);
                $tasksList = CRestPlus::callBatchList('tasks.task.list', array(
                    'order' => array(
                        'CREATED_DATE' => 'asc'
                    ),
                    'filter' => array(
                        'ID' => $tuIDs
                    )
                ));
                foreach ($tasksList['result']['result'] as $tlPackages) {
                    foreach ($tlPackages['tasks'] as $tlItems) {
                        if (is_array($tlItems['ufCrmTask'])) {
                            $explodedDI = explode('D_', $tlItems['ufCrmTask'][0]);
                            $dealsIDs[] = $explodedDI[1];
                        }
                    }
                }
                if (($_POST['direction'] === 'empty') and (($_POST['stage'] === 'empty'))) {
                    $dlCall = CRestPlus::callBatchList('crm.deal.list', array(
                        'ORDER' => array(
                            'DATE_CREATE' => 'asc'
                        ),
                        'FILTER' => array(
                            'ID' => $dealsIDs
                        )
                    ));
                } elseif (($_POST['direction'] === 'empty') and ($_POST['stage'] !== 'empty')) {
                    $stageID[] = $_POST['stage'];
                    foreach ($dealsCats as $eDC => $iDC) {
                        $stageID[] = 'C' . $eDC . ':' . $_POST['stage'];
                    }
                    $dlCall = CRestPlus::callBatchList('crm.deal.list', array(
                        'ORDER' => array(
                            'DATE_CREATE' => 'asc'
                        ),
                        'FILTER' => array(
                            'ID' => $dealsIDs,
                            'STAGE_ID' => $stageID
                        )
                    ));
                } elseif (($_POST['direction'] !== 'empty') and ($_POST['stage'] === 'empty')) {
                    $dlCall = CRestPlus::callBatchList('crm.deal.list', array(
                        'ORDER' => array(
                            'DATE_CREATE' => 'asc'
                        ),
                        'FILTER' => array(
                            'ID' => $dealsIDs,
                            'CATEGORY_ID' => $_POST['direction']
                        )
                    ));
                } else {
                    $stageID[] = $_POST['stage'];
                    foreach ($dealsCats as $eDC => $iDC) {
                        $stageID[] = 'C' . $eDC . ':' . $_POST['stage'];
                    }
                    $dlCall = CRestPlus::callBatchList('crm.deal.list', array(
                        'ORDER' => array(
                            'DATE_CREATE' => 'asc'
                        ),
                        'FILTER' => array(
                            'ID' => $dealsIDs,
                            'STAGE_ID' => $stageID,
                            'CATEGORY_ID' => $_POST['direction']
                        )
                    ));
                }
                foreach ($dlCall['result']['result'] as $packagesDC) {
                    foreach ($packagesDC as $packagesItems) {
                        $cdlIDsWL[] = 'D_' . $packagesItems['ID'];
                    }
                }
                foreach ($tasksList['result']['result'] as $item) {
                    foreach ($item['tasks'] as $task) {
                        foreach ($cdlIDsWL as $ciwValue) {
                            if (in_array($ciwValue, $task['ufCrmTask'])) {
                                $tasksWL[] = $task['id'];
                            } elseif (empty($task['ufCrmTask'])) {
                                $tasksWL[] = $task['id'];
                            }
                        }
                    }
                }
                $twlUnique = array_unique($tasksWL);
                foreach ($tasksTime as $itemTT) {
                    foreach ($itemTT['result'] as $valueTT) {
                        foreach ($twlUnique as $twlValue) {
                            if (in_array($twlValue, $valueTT)) {
                                $teWL[] = $valueTT;
                            }
                        }
                    }
                }
                $tasksTime = [];
                if ($dlCall === 'error') {
                    $stageNF = true;
                }
            } else {
                $responsibleNF = true;
            }
        }
    } else {
        $stageNF = true;
    }
}
require_once 'query.php';