<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Отчет по затраченному сотрудниками времени в задачах</title>
    <? require_once 'bitrixStyles.php' ?>
    <style>
        .parent {
            width: 100%;
            text-align: center;
            margin: auto;
        }

        .child {
            display: inline-block;
            padding: 16px;
            box-shadow: 0 0 4px rgba(0, 0, 0, 0.25);
            margin: 8px;
        }

        #reportTable {
        <?
            if ((!is_null($_POST['author'])) and ($responsibleNF !== true) and ($stageNF !== true)) {
                echo "display: inline;";
            } else {
                echo "display: none;";
            }
        ?>
        }

        table {
            text-align: center;
            border-collapse: collapse;
        }

        td, th {
            border: 1px solid #8698A1;
            text-align: center;
            padding: 8px;
            font-weight: normal;
        }

        .hoverable:hover {
            background: rgba(32, 103, 176, 0.075);
        }

        /*.odd {
            background: #F5F5F5;
        }*/

        .bxBlueBG {
            background: #B6E7F8;
        }

        .bxGreyBG {
            background: #ECF0F4;
        }

        select, input[type='date'] {
            height: 40px;
            padding: 8px;
            margin: 8px;
        }

        input[type='date'] {
            height: 20px;
        }

        .redContainer {
            background-color: #F54819;
            color: #FFF;
            padding: 16px;
            margin: auto;
            width: 50%;
            line-height: 1.5;
        }

        .redInfoBlock, .orangeInfoBlock, .greenInfoBlock{
            background-color: #F1361B;
            padding: 16px;
            width: 25%;
            line-height: 1.5;
            text-align: center;
            margin: 16px auto 16px auto;
            color: #FFF;
        }

        .orangeInfoBlock {
            background-color: #FFE75E;
            color: #000;
        }

        .greenInfoBlock {
            background-color: #BBED21;
            color: #000;
        }

        .rcLink {
            cursor: pointer;
            border-bottom: 1px dashed #FFF;
        }
    </style>
    <script src="//api.bitrix24.com/api/v1/"></script>
    <script>
        BX24.init(function () {
            BX24.installFinish();
        });
    </script>
    <script>
        function stageFocus() {
            document.getElementById('stageSelect').focus();
        }

        function userFocus() {
            document.getElementById('userSelect').focus();
        }

        function directionFocus() {
            document.getElementById('directionSelect').focus();
        }

        function periodFocus() {
            document.getElementById('period').focus();
        }
    </script>
</head>
<body>
<?
if (INFO_BLOCK == '1') {
    echo '
    <div class="redInfoBlock">
        ВНИМАНИЕ! Приложение находится в стадии разработки
    </div>';
} elseif (INFO_BLOCK == '2') {
    echo '
    <div class="orangeInfoBlock">
        ВНИМАНИЕ! Приложение находится в стадии дебаггинга
    </div>';
} elseif (INFO_BLOCK == '3') {
    echo '
    <div class="greenInfoBlock">
        ВНИМАНИЕ! Приложение находится в стадии бета-тестирования
    </div>';
}
if ($responsibleNF === true) {
    echo "<div class='redContainer'>
    Данные не найдены. Возможные причины:<br>
    &mdash; данные по выбранному <span onclick='userFocus()' class='rcLink'>пользователю</span> не найдены<br>
    &mdash; данные за указанный <span onclick='periodFocus()' class='rcLink'>период</span> не найдены
    </div>";
} elseif ($stageNF === true) {
    echo "<div class='redContainer'>
    Данные не найдены. Возможные причины:<br>
    &mdash; данные по выбранной <span onclick='stageFocus()' class='rcLink'>стадии</span> или <span onclick='directionFocus()' class='rcLink'>направлению</span> сделки не найдены<br>
    &mdash; данные за указанный <span onclick='periodFocus()' class='rcLink'>период</span> не найдены<br>
    &mdash; по задаче нет списаний времени
</div>";
}
?>
<div class="parent">
    <form method="post" id="getOrder">
        <div class="child">
            <label>
                Выберите автора (ответственного):<br>
                <select name="author" id="userSelect">
                    <option value="empty">Не выбран</option>
                    <?php
                    foreach ($users as $uKey => $uItem) {
                        if ((!is_null($_POST['author'])) and ($uKey == $_POST['author']) and ($_POST['author'] != 'empty')) {
                            echo "<option value='$uKey' selected>$uItem</option>";
                        } else {
                            echo "<option value='$uKey'>$uItem</option>";
                        }
                    }
                    ?>
                </select>
            </label>
        </div>
        <div class="child">
            <label>
                Выберите период:<br>
                <?php
                if (!is_null($_POST['dateStart']) and (!is_null($_POST['dateEnd']))) {
                    echo '<input type="date" value="' . $_POST['dateStart'] . '" name="dateStart" id="period" requried>';
                    echo '<input type="date" value="' . $_POST['dateEnd'] . '" name="dateEnd" requried>';
                } else {
                    echo '<input type="date" value="' . LAST_MONTH . '" name="dateStart" id="period" requried>';
                    echo '<input type="date" value="' . CURRENT_DATE . '" name="dateEnd" requried>';
                }
                ?>
            </label>
        </div>
        <div class="child">
            <label>
                Выберите стадию сделки:<br>
                <select name="stage" id="stageSelect">
                    <option value="empty" selected>Не выбрано</option>
                    <?php
                    foreach ($statIDs as $sidsKey => $sidsItem) {
                        if ((!is_null($_POST['stage'])) and ($sidsKey == $_POST['stage']) and ($_POST['stage'] != 'empty')) {
                            echo "<option value='$sidsKey' selected>$sidsItem</option>";
                        } else {
                            echo "<option value='$sidsKey'>$sidsItem</option>";
                        }
                    }
                    ?>
                </select>
            </label>
        </div>
        <div class="child">
            <label>
                Выберите направление сделки:<br>
                <select name="direction" id="directionSelect">
                    <option value="empty" selected>Не выбрано</option>
                    <?php
                    foreach ($dealsCats as $dctsKey => $dctsItem) {
                        if ((!is_null($_POST['direction'])) and ($dctsKey == $_POST['direction']) and ($_POST['direction'] != 'empty')) {
                            echo "<option value='$dctsKey' selected>$dctsItem</option>";
                        } else {
                            echo "<option value='$dctsKey'>$dctsItem</option>";
                        }
                    }
                    ?>
                </select>
            </label>
        </div>
        <p><input type="submit" class="ui-btn ui-btn-primary" value="Показать"><?
            if ((!is_null($_POST['author'])) and ($responsibleNF !== true) and ($stageNF !== true)) {
                echo '<a href="excel.php" class="ui-btn ui-btn-primary">Выгрузить в CSV</a></p>';
            } ?></p>
    </form>
</div>
<table id="reportTable">
    <thead>
    <tr class="bxBlueBG">
        <th colspan="6" class="grey">Информация по сделке</th>
        <th colspan="5" class="grey">Информация по задаче</th>
        <th colspan="4" class="grey">Расшифровка списания времени</th>
    </tr>
    <tr class="bxGreyBG">
        <th class="blue">Направление сделки</th>
        <th class="blue">Id сделки</th>
        <th class="blue">Сделка</th>
        <th class="blue">Дата начала сделки</th>
        <th class="blue">Дата закрытия сделки</th>
        <th class="blue">Стадия сделки</th>
        <th class="blue">Id задачи</th>
        <th class="blue">Задача</th>
        <th class="blue">Стадия задачи (закрыта/ не закрыта)</th>
        <th class="blue">Норма времени по задаче</th>
        <th class="blue">Списано времени по задаче</th>
        <th class="blue">Дата (время)</th>
        <th class="blue">Автор</th>
        <th class="blue">Время (списано)</th>
        <th class="blue">Комментарий (расшифровка что сделано)</th>
    </tr>
    </thead>
    <tbody>
    <?
    if ((!is_null($_POST['author'])) and ($responsibleNF !== true) and ($stageNF !== true)) {
        ### Наполенение таблицы ###
        foreach ($teWL as $csvKey => $ttItem) {
            foreach ($tasksList['result']['result'] as $tlItem) {
                foreach ($tlItem['tasks'] as $tliKey => $tlTItem) {
                    if ($tlTItem['id'] === $ttItem['TASK_ID']) {
                        if (($tliKey % 2) == 0) {
                            echo '<tr class="hoverable odd">';
                        } else {
                            echo '<tr class="hoverable">';
                        }
                        if (is_array($tlTItem['ufCrmTask'])) {
                            $explodedTLTI = explode('D_', $tlTItem['ufCrmTask'][0]);
                            foreach ($dlCall['result']['result'] as $dcPackage) {
                                foreach ($dcPackage as $dcItem) {
                                    if ($dcItem['ID'] == $explodedTLTI[1]) {
                                        foreach ($dealsCats as $dcCatsKeys => $dcCatsItems) {
                                            if ($dcItem['CATEGORY_ID'] == $dcCatsKeys) {
                                                echo '<td>' . $dcCatsItems . '</td>';
                                                $csvArr[$csvKey]['dealDirection'] = $dcCatsItems;
                                            }
                                        }
                                        echo '<td>' . $dcItem['ID'] . '</td>';
                                        echo '<td>' . '<a href="https://' . DOMAIN . '/crm/deal/details/' . $dcItem['ID'] . '/" target="_blank">' . $dcItem['TITLE'] . '</a></td>';
                                        $csvArr[$csvKey]['dealID'] = $dcItem['ID'];
                                        $csvArr[$csvKey]['dealName'] = $dcItem['TITLE'];
                                        $csvArr[$csvKey]['dealLink'] = 'https://' . DOMAIN . '/crm/deal/details/' . $dcItem['ID'] . '/';
                                        $sliceDC = substr($dcItem['DATE_CREATE'], 0, -9);
                                        $explodedDC = explode('T', $sliceDC);
                                        $formatedDC = date('d.m.Y', strtotime($dcItem['DATE_CREATE']));
                                        echo '<td>' . $formatedDC . '</td>';
                                        $csvArr[$csvKey]['dealStartDate'] = $formatedDC;
                                        if ($dcItem['CLOSEDATE'] == '') {
                                            echo '<td>&mdash;</td>';
                                            $csvArr[$csvKey]['dealCloseDate'] = '';
                                        } else {
                                            $sCloseDate = substr($dcItem['CLOSEDATE'], 0, -9);
                                            $eCloseDate = explode('T', $sCloseDate);
                                            $formatedDC = date('d.m.Y', strtotime($dcItem['CLOSEDATE']));
                                            echo '<td>' . $formatedDC . '</td>';
                                            $csvArr[$csvKey]['dealCloseDate'] = $formatedDC;
                                        }
                                        if (preg_match('~:~', $dcItem['STAGE_ID'])) {
                                            $explodedSI = explode(':', $dcItem['STAGE_ID']);
                                            $stageID = $explodedSI[1];
                                        } else {
                                            $stageID = $dcItem['STAGE_ID'];
                                        }
                                        $stageName = $statIDs[$dcItem['STAGE_ID']];
                                        echo '<td>' . $stageName . '</td>';
                                        $csvArr[$csvKey]['dealStage'] = $stageName;
                                        ### Парсинг задачи ###
                                        echo '<td>' . $tlTItem['id'] . '</td>';
                                        echo '<td>' . '<a href="https://' . DOMAIN . '/company/personal/user/' . $curUsr['result']['ID'] . '/tasks/task/view/' . $tlTItem['id'] . '/" target="_blank">' . $tlTItem['title'] . '</a></td>';
                                        $csvArr[$csvKey]['taskID'] = $tlTItem['id'];
                                        $csvArr[$csvKey]['taskLink'] = 'https://' . DOMAIN . '/company/personal/user/' . $curUsr['result']['ID'] . '/tasks/task/view/' . $tlTItem['id'] . '/';
                                        if ($tlTItem['status'] === '5') {
                                            $stageStat = 'Закрыта';
                                        } elseif ($tlTItem['status'] === '2') {
                                            $stageStat = 'Открыта';
                                        }
                                        echo '<td>' . $stageStat . '</td>';
                                        $csvArr[$csvKey]['taskStage'] = $stageStat;
                                        if ($tlTItem['timeEstimate'] == '0') {
                                            echo '<td>&mdash;</td>';
                                            $csvArr[$csvKey]['taskTimeEstimate'] = '';
                                        } else {
                                            $calcTimeE = round(($tlTItem['timeEstimate'] / 60 / 60), 2);
                                            echo '<td>' . $calcTimeE . '</td>';
                                            $csvArr[$csvKey]['taskTimeEstimate'] = (string)str_replace('.', ',', $calcTimeE);
                                        }
                                        $calcTimeSIL = round(($tlTItem['timeSpentInLogs'] / 60 / 60), 2);
                                        echo '<td>' . $calcTimeSIL . '</td>';
                                        $csvArr[$csvKey]['taskWrittenTime'] = (string)str_replace('.', ',', $calcTimeSIL);
                                        ### Парсинг списания времени ###
                                        $sliceCD = substr($ttItem['CREATED_DATE'], 0, -9);
                                        $explodedCD = explode('T', $sliceCD);
                                        $formatedDC = date('d.m.Y', strtotime($ttItem['CREATED_DATE']));
                                        echo '<td>' . $formatedDC . ' (' . $explodedCD[1] . ')' . '</td>';
                                        $csvArr[$csvKey]['commentDate'] = $formatedDC . ' (' . $explodedCD[1] . ')';
                                        foreach ($users as $uKey => $uValue) {
                                            if ($uKey == $ttItem['USER_ID']) {
                                                echo '<td>' . $uValue . '</td>';
                                                $csvArr[$csvKey]['commentAuthor'] = $uValue;
                                            }
                                        }
                                        $toHours = round(($ttItem['MINUTES'] / 60), 2);
                                        echo '<td>' . $toHours . '</td>';
                                        if ($ttItem['COMMENT_TEXT'] !== '') {
                                            echo '<td>' . $ttItem['COMMENT_TEXT'] . '</td>';
                                        } else {
                                            echo '<td>&mdash;</td>';
                                        }
                                        $csvArr[$csvKey]['commentWrittenTime'] = (string)str_replace('.', ',', $toHours);
                                        $csvArr[$csvKey]['commentText'] = $ttItem['COMMENT_TEXT'];
                                        echo '</tr>';
                                    }
                                }
                            }
                        } else {
                            echo '<td>&mdash;</td>';
                            echo '<td>&mdash;</td>';
                            echo '<td>&mdash;</td>';
                            echo '<td>&mdash;</td>';
                            echo '<td>&mdash;</td>';
                            echo '<td>&mdash;</td>';
                            $csvArr[$csvKey]['dealDirection'] = '';
                            $csvArr[$csvKey]['dealID'] = '';
                            $csvArr[$csvKey]['dealName'] = '';
                            $csvArr[$csvKey]['dealLink'] = '';
                            $csvArr[$csvKey]['dealStartDate'] = '';
                            $csvArr[$csvKey]['dealCloseDate'] = '';
                            $csvArr[$csvKey]['dealStage'] = '';
                            ### Парсинг задачи ###
                            echo '<td>' . $tlTItem['id'] . '</td>';
                            echo '<td>' . '<a href="https://' . DOMAIN . '/company/personal/user/' . $curUsr['result']['ID'] . '/tasks/task/view/' . $tlTItem['id'] . '/" target="_blank">' . $tlTItem['title'] . '</a></td>';
                            $csvArr[$csvKey]['taskID'] = $tlTItem['id'];
                            $csvArr[$csvKey]['taskLink'] = 'https://' . DOMAIN . '/company/personal/user/' . $curUsr['result']['ID'] . '/tasks/task/view/' . $tlTItem['id'] . '/';
                            if ($tlTItem['status'] === '5') {
                                $stageStat = 'Закрыта';
                            } elseif ($tlTItem['status'] === '2') {
                                $stageStat = 'Открыта';
                            }
                            echo '<td>' . $stageStat . '</td>';
                            $csvArr[$csvKey]['taskStage'] = $stageStat;
                            if ($tlTItem['timeEstimate'] == '0') {
                                echo '<td>&mdash;</td>';
                                $csvArr[$csvKey]['taskTimeEstimate'] = '';
                            } else {
                                $calcTimeE = round(($tlTItem['timeEstimate'] / 60 / 60), 2);
                                echo '<td>' . $calcTimeE . '</td>';
                                $csvArr[$csvKey]['taskTimeEstimate'] = (string)str_replace('.', ',', $calcTimeE);
                            }
                            $calcTimeSIL = round(($tlTItem['timeSpentInLogs'] / 60 / 60), 2);
                            echo '<td>' . $calcTimeSIL . '</td>';
                            $csvArr[$csvKey]['taskWrittenTime'] = (string)str_replace('.', ',', $calcTimeSIL);
                            ### Парсинг списания времени ###
                            $sliceCD = substr($ttItem['CREATED_DATE'], 0, -9);
                            $explodedCD = explode('T', $sliceCD);
                            $formatedDC = date('d.m.Y', strtotime($ttItem['CREATED_DATE']));
                            echo '<td>' . $formatedDC . ' (' . $explodedCD[1] . ')' . '</td>';
                            $csvArr[$csvKey]['commentDate'] = $formatedDC . ' (' . $explodedCD[1] . ')';
                            foreach ($users as $uKey => $uValue) {
                                if ($uKey == $ttItem['USER_ID']) {
                                    echo '<td>' . $uValue . '</td>';
                                    $csvArr[$csvKey]['commentAuthor'] = $uValue;
                                } else {
                                    echo '<td>&mdash;</td>';
                                    $csvArr[$csvKey]['commentAuthor'] = '';
                                    break;
                                }
                            }
                            $toHours = round(($ttItem['MINUTES'] / 60), 2);
                            echo '<td>' . $toHours . '</td>';
                            if ($ttItem['COMMENT_TEXT'] !== '') {
                                echo '<td>' . $ttItem['COMMENT_TEXT'] . '</td>';
                            } else {
                                echo '<td>&mdash;</td>';
                            }
                            $csvArr[$csvKey]['commentWrittenTime'] = (string)str_replace('.', ',', $toHours);
                            $csvArr[$csvKey]['commentText'] = $ttItem['COMMENT_TEXT'];
                            echo '</tr>';
                        }
                    }
                }
            }
        }
        Debugger::saveParams($csvArr, 'settings.php');
    }
    ?>
    <tr class="bxBlueBG">
        <td colspan="15">Конец</td>
    </tr>
    </tbody>
</table>
<?
/*if ((!is_null($_POST['author'])) and ($responsibleNF !== true) and ($stageNF !== true)) {
    echo '<hr>' . 'Отладочная информация:' . '<br>';
    ### Отладка ниже ###
    Debugger::debug($statIDs);
}*/
?>
</body>
</html>